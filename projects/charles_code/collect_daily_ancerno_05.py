## (toc:PArse Agrs)
# good tutorial here:
# https://docs.python.org/2/howto/argparse.html
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("year", help="year to parse")
parser.add_argument("--pool", default="US", help="pool to extract [US|Non US]")
args = parser.parse_args()

# usage: convert_one_year.py 2010 [01,02,03]
print("----------------------------------------------")
str_year = args.year  # sys.argv[1]
pool = args.pool

print("%s|%s" % (str_year, pool))

year = str_year

## (toc:Configuration and Imports)
import os
import glob
import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

## (toc:Get Universe)
json_universe = fdir.replace('%s Trading Equity Data/' % pool,
                             '%s_stocks_1997-2015_found.json' % pool.replace(' ', '_'))
df_universe = pds.read_json( json_universe)
print("Universe of %d stocks to concat..." % df_universe.shape[0])

## (toc:Loop on stocks)
import warnings
warnings.filterwarnings('ignore', category=tables.NaturalNameWarning)

fnames = glob.glob(fdir + year + '/*_split.pk')
for i, fname in enumerate(fnames):
    day_str = fname.split('/')[-1].split('_')[0]
    print("working on <%s:%d/%d>..." % (day_str, i, len(fnames)))
    df_daily = pds.read_pickle( fname)
    symb_found = []
    for symb in df_universe.code:
        df_this_symb = df_daily[df_daily.Symbol==symb]
        if df_this_symb.shape[0] > 0:
            storage = pds.HDFStore(fdir + year + ('/stock_%s.hd5' % symb))
            storage[day_str] = df_this_symb
            storage.close()
            symb_found.append( symb)
    print("... exported %d names" % (len(symb_found)))
##  (toc:Store them properly)
all_stock_files = glob.glob(fdir + year + '/stock_*.hd5')
for i, fcheck in enumerate(all_stock_files):
    print("merging <%s:%s>..." % (year, fcheck.split('/')[-1])) 
    storage = pds.HDFStore(fcheck)
    df_all = pds.DataFrame()
    for day_str in storage.keys():
        df_day = storage[day_str]
        df_all = pds.concat([df_all, df_day], ignore_index=True)
    storage.close()
    df_all.to_pickle(fcheck.replace('.hd5', '.pkl').replace('stock_', 'merged_stock_'))
    print(" ... %d/%d." % (i, len(all_stock_files)))
## END
