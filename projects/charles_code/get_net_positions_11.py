## Get net positions computed by 10.py

# [toc:
# * Imports and directories
#   * PReprocessings
# * Worst cases
#   * Simple ref stats
#   * Identify extremes
# * Select worst cases
#   * Explore
#   * Plot thresholding options
#   * Definite subset
# ]

pool = 'US'

# (toc:Imports and directories)
import glob
import os

import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

# (toc:Preprocessings:-)
all_stored = fdir + 'net_positions.h5'
storage = pds.HDFStore(all_stored)
df_all_years = storage['df_all_years']
storage.close()
df_all_years = df_all_years.set_index('TradeDate')
df_all_years['year'] = [d.year for d in df_all_years.index.values]
df_all_years['ratio'] = df_all_years['net'] / df_all_years['sum']

df_recent = df_all_years[df_all_years.index > pds.to_datetime('2012-01-01').date()]

## (toc:Worst cases)

# (toc:Simple ref stats:-)
df_avg_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio', 'sum']].mean()
df_std_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio', 'sum']].std()
df_count_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio']].count().rename(columns={'ratio': 'year (count)'})

df_ = pds.merge(df_std_ratio, df_avg_ratio,
                left_index=True, right_index=True,
                how='outer', suffixes=[' (std)', ' (avg)'])
df_ = pds.merge(df_, df_count_ratio,
                left_index=True, right_index=True,
                how='outer')

df_recent = pds.merge(df_recent.reset_index(), df_.reset_index(),
                left_on=['Symbol', 'year'], right_on=['Symbol', 'year'],
                how='left')

## (toc:Identify extremes:-)
df_recent['abnormal sum'] = (df_recent['sum'] / df_recent['sum (avg)']) 
df_recent['abnormal ratio'] = (df_recent['ratio'] - df_recent['ratio (avg)']) / df_recent['ratio (std)']

# Take the most intense ratios
ind_sr = np.argsort(-np.abs(df_recent['abnormal ratio'].values))
# df_recent.ix[df_recent.index[ind_s]]
df_recent['ranked ab.ratio'] = ind_sr

# Take the most intense ratios
ind_ss = np.argsort(-np.abs(df_recent['abnormal sum'].values))
# df_recent.ix[df_recent.index[ind_s]]
df_recent['ranked ab.sum'] = ind_ss

## (toc:Select worst cases)

# (toc:Explore:-)
sum_thresholds = np.linspace(2, 20, 20)
lst_test_thres = []
for K in [200, 500, 800, 1000, 1500, 2000]:
    df_select = df_recent.sort('abnormal ratio', inplace=False, ascending=False).head(K)
    lst_test_thres.append(pds.DataFrame({'threshold': sum_thresholds, 'K': K,
                                         'avg ratio': [np.abs(df_select[df_select['abnormal sum'] > t]['abnormal ratio']).mean() \
                                                       for t in sum_thresholds],
                                         'nbe': [(df_select['abnormal sum'] > t).sum() \
                                                 for t in sum_thresholds] }))
df_test_thres = pds.concat(lst_test_thres, axis=0)
                          
## (toc:Plot thresholding options:-)
import matplotlib.pyplot as plt

fig = plt.figure(0)
plt.clf()

# cm=cplot.colormap_from_list(lst_k, colorscheme='RdBu')

ax1 = plt.subplot(2, 1, 1)
df_test_thres.groupby(['threshold', 'K'])['nbe'].min().unstack().fillna(0).plot(lw=4, alpha=.85, ax=ax1)
plt.ylabel('Nbe of stocks\ns.t. abnorm(sum) > threshold', size=18)
plt.grid(True)

ax2 = plt.subplot(2, 1, 2)
df_test_thres.groupby(['threshold', 'K'])['avg ratio'].min().unstack().fillna(0).plot(lw=4, alpha=.85, ax=ax2)
plt.ylabel('Avg. |ratio| abnormality', size=18)
ax2.legend_.remove()
plt.grid(True)

plt.tight_layout()
# plt.subplots_adjust(right=0.95)
if False:
    img_name = 'p11_choose_threshold'
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % img_name)
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % img_name)

## (toc:Definite subset:-)
# I choose
K = 900
t = 2.5

df_select = df_recent.sort('abnormal ratio', inplace=False, ascending=False).head(K)
df_select = df_select[df_select['abnormal sum'] > t]

fname = '/home/ma/c/clehalle/dev_python/projects/ancerno/data/p11_select_worst_2012.h5'
storage = pds.HDFStore(fname)
storage['df_select'] = df_select
storage.close()
df_select.to_json(fname.replace('.h5', '.json'))

## END
