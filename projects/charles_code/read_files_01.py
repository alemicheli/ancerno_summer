## Convert files in pk
## (toc:Use numpy txt reader)
# This is the good solution:
# 1. use numpy file reader (it knows how to handle \x0)
# 2. then convert empty spaces to NaNs
import os

#ancerno_dir = os.environ['ANCERNO']
ancerno_dir = '/home/ma/a/am1118'
original_data = '/data/original/US Trading Equity Data/'

## (toc:LIB)
def destination_file(fpath):
    import distutils.dir_util
    dpath = fpath.replace('original', 'tmp')
    dname = os.path.dirname(dpath)
    distutils.dir_util.mkpath(dname)
    return dpath, dname
  

def convert_file(uncomp_fname):
    import numpy as np
    import pandas as pds
    ar_data = np.genfromtxt(uncomp_fname, delimiter='|',
                            dtype=None, names=True, missing_values=np.nan)

    df_data = pds.DataFrame(ar_data)
    df_data_with_nans = df_data.replace(r'', np.nan, regex=True)
    for c in df_data_with_nans.columns:
        if c.find('dt') != 1:
            continue
        df_data_with_nans[c] = pds.to_datetime(df_data_with_nans[c])

    df_data_with_nans.to_pickle(uncomp_fname.replace('txt', 'pk'))
    return df_data_with_nans.shape

## Unzip
import glob

for dname in glob.glob(ancerno_dir + original_data + '*'):
    year = int(dname.split('/')[-1])
    print("working on year %d" % (year))
    if year != 2002:
        print('   waiting for 2002...')
        continue
    for fname in glob.glob(dname + '/*.txt.gz'):
        if fname.split('/')[-1] in ['Trades201202.txt.gz']:
            print("[%s] already processed... skipping" % fname.split('/')[-1])
            continue
        dest_fname, dest_dir = destination_file(fname)
        print("%s --> %s" % (fname, dest_dir))
        print("   UNZIP")
        os.system('unzip -d "%s/" "%s"' % (dest_dir, fname))
        print("   SPLIT")
        new_fname = fname.replace('.gz', '').split('/')[-1]
        os.system('cd "%s";awk -f ~/ancerno_drive/projects/ancerno/split_by_day.awk %s' % (dest_dir, new_fname))
        print("   CONVERT")
        for split_fname in glob.glob(dest_dir + '/*_split.txt'):
            print("   ... work on <%s>" % split_fname)
            #if True:  
            try:
                shape = convert_file(split_fname)
                print("   ___ %d x %d" % shape)
            except:
                with open("%s/problems.txt" % dest_dir, "a") as problems:
                   problems.write("problem with file <%s>\n" % split_fname)
        print("   REMOVE")
        os.system('cd "%s";rm *_split.txt' % dest_dir)
        os.system('cd "%s";rm Trades*.txt' % dest_dir)


## END
