## Simple stats to find the most liquid stocks / guys
pool = 'Non US'

import os
import glob
import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

## (toc:LIB)

def groupby_daily(df_data):
    return df_data.groupby('Symbol')[['Value', 'has_taxes', 'count']].sum().sort_values(by='Value', ascending=False)

def reduce_day(df_data):
    df_data['Value'] = df_data['Price'] * df_data['Volume']
    df_data['has_taxes'] = (df_data['TaxesAndFees']>0).astype(int)
    df_data['count'] = 1
    df_summary = groupby_daily(df_data)

    print("Reduction from %d rows to %d ones..." % (df_data.shape[0], df_summary.shape[0]))

    return df_summary
    
##
fnames = glob.glob( fdir + '/*.pkl')
print("%d files found" % len(fnames))

df_all_summaries = pds.DataFrame()
for i, fname in enumerate(fnames):
    print("%d/%d -- %s" % (i, len(fnames), fname.split('/')[0]))
    df_summary = pds.read_pickle(fname)
    df_concat  = pds.concat([df_all_summaries, df_summary], axis=0, ignore_index=True)
    df_all_summaries = groupby_daily(df_concat.reset_index()).reset_index()
df_all_summaries = df_all_summaries.sort_values(by="Value", ascending=False)
print(df_all_summaries.shape)

ts_dates = pds.to_datetime([f.split('/')[-1].split('_')[0] for f in fnames])
dates_range = (str(np.min(ts_dates))[:4], str(np.max(ts_dates))[:4])
##
fname_out = 'stocks_%s-%s.pk' % dates_range
fout_pk = fdir.replace('%s Trading Equity Data/' % pool,
                       '%s_' % pool).replace(' ', '_') + fname_out
print('STORE --> %s' % fout_pk)
storage = pds.HDFStore(fout_pk)
storage['df_all_dummaries'] = df_all_summaries
storage.close()
##
fout_json = fout_pk.replace('.pk', '.json')
df_all_summaries.to_json(fout_json)
print('json storage: %s' % fout_json)
##
K = 30
ts_dates = pds.to_datetime([f.split('/')[-1].split('_')[0] for f in fnames])
dates_range = (str(np.min(ts_dates))[:4], str(np.max(ts_dates))[:4])

import matplotlib.pyplot as plt

f = plt.figure(0)
plt.clf()

ax1 = plt.subplot(1, 2, 2)
plt.plot(df_all_summaries['count'], df_all_summaries['has_taxes'], 'ok', alpha=.35, markersize=16)
plt.xlabel('Nbe of metaorders', size=16)
plt.ylabel('Nbe of non zero taxes', size=16)
plt.title('Slope: %.1f%%' % (100. * df_all_summaries['has_taxes'] / df_all_summaries['count']).mean(), size=18)
plt.grid(True)


ax2 = plt.subplot(1, 2, 1)
y = K - np.arange(K)
plt.barh(y, df_all_summaries['Value'].values[:K] / 1e9, align='center')
plt.yticks(y, df_all_summaries['Symbol'].values[:K])
plt.title('Most traded (in 1e9 dollars) [%s-%s]' % dates_range, size=18)
plt.grid(True)
plt.ylim((.5, K + .5))

plt.tight_layout()

if True:
    figname = 'p04_count_stocks_%s_%s-%s' % (pool.replace(' ','_'), dates_range[0], dates_range[1])
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % figname)
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % figname)


## END
