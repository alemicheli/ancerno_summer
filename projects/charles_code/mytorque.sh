#!/bin/bash

## To be used like this
# qsub -v SCRIPT='/home/ma/c/clehalle/dev_python/projects/ancerno/merge_daily_data_by_year.py',ARGS=2013 ./mytorque.sh -o /home/ma/c/clehalle/logs/merge_daily_data_by_year_2013.txt -N MDDbY2013

# to look at jobs: http://torque.ma.ic.ac.uk/

## did this before
# http://sysnews.ma.ic.ac.uk/compute-cluster/#ssh_setup

## I tried that, but do not use it in this script at this stage...
# echo $SCRIPT $ARGS '>' $HOME/logs/${SCRIPT##*/}_${ARGS//[[:blank:]]/}.txt
. /home/ma/c/clehalle/torque/bin/activate

echo $SCRIPT $ARGS '>' $ANCERNO

$SCRIPT $ARGS
