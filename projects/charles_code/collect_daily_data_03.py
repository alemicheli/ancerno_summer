## Simple stats to find the most liquid stocks / guys
import os
import glob
import pandas as pds

fdir = os.environ['ANCERNO'] + '/data/tmp/Non US Trading Equity Data/'
year = '2003'

## (toc:LIB)

def groupby_daily(df_data):
    return df_data.groupby('Symbol')[['Value', 'has_taxes', 'count']].sum().sort_values(by='Value', ascending=False)

def reduce_day(df_data, verbose=0):
    if (df_data.shape[0] <= 0) or ('Price' not in df_data.columns):
        import pandas as pds
        return pds.DataFrame()
    df_data['Value'] = df_data['Price'] * df_data['Volume']
    df_data['has_taxes'] = (df_data['TaxesAndFees']>0).astype(int)
    df_data['count'] = 1
    df_summary = groupby_daily(df_data)
    if verbose > 0:
        print("Reduction from %d rows to %d ones..." % (df_data.shape[0], df_summary.shape[0]))

    return df_summary

## Read CFM known stocks
import pandas as pds
fref = '/home/ma/c/clehalle/dev_python/projects/ancerno/ref_ukeujp.pk'
df_ref_md = pds.read_pickle(fref).rename(columns={'code':'Symbol'})
df_ref_md['alt_Symbol'] = [(s[1:] if s[0]=='@' else s) for s in df_ref_md.Symbol]

##
import numpy as np
packets_size = 60

fnames = glob.glob( fdir + '*/*_split.pk')
print("==> WILL WORK ON [%d] FILES <==" % len(fnames))

df_all_summaries = pds.DataFrame()
for i, fname in enumerate(fnames):
    df_data = pds.read_pickle(fname)
    # df_summary = reduce_day(df_data).reset_index().ix[:select_bests]
    df_summary = reduce_day(df_data).reset_index()
    if df_summary.shape[0] <= 0:
        print('   Nothing to do with <%s>' % fname)
        print('   ... skipping')
        continue
    nbe_symbols = df_summary.shape[0]
    # only keep symbols that are in my ref list
    df_summary = df_summary[np.logical_or(df_summary['Symbol'].isin(df_ref_md.Symbol),
                                          df_summary['Symbol'].isin(df_ref_md.alt_Symbol))]
    df_concat  = pds.concat([df_all_summaries, df_summary], axis=0, ignore_index=True)
    df_all_summaries = groupby_daily(df_concat.reset_index()).reset_index()

    if i % packets_size == 0:
        print('==> reducing from %d to %d stocks...' % (nbe_symbols, df_summary.shape[0]))
        print('    for now I have %d stocks in my list...' % df_all_summaries.shape[0])
        df_all_summaries.to_pickle(fdir + 'temp_seen_stocks.pk')

df_all_summaries.to_pickle(fdir + 'seen_stocks.pk')
df_all_summaries.to_json(fdir + 'seen_stocks.json')

## END
