## Explore the content of variable

# from __future__ import print_function

pool = 'US'
year = '2012'

## (toc:Imports and directories
import glob
import os

import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

## (toc:Loop on years)
lst_all_years = []
for fyear in glob.glob(fdir + '2*/') + glob.glob(fdir + '1*/'):
    year = fyear.split('/')[-2]
    fnames = glob.glob(fyear + '/merged_*.pkl')  # fdir + year
    print('On year %s, universe is made of %d stocks' % (year, len(fnames)))
    # (toc:Loop on stocks)
    lst_positions = []
    for fname in fnames:
        df = pds.read_pickle(fname)
        if df.shape[0] < 10:
            print('Not enough days on <%s>' % '/'.join(fname.split('/')[-2:]))
            continue
        df['signed dv'] = df.Side * df.dv
        gr = df.groupby(pds.to_datetime(df.TradeDate).dt.date)
        df_pos = pds.DataFrame({'sum': gr['dv'].sum(), 'net': gr['signed dv'].sum(), 'count': gr['dv'].count()})
        df_pos['Symbol'] = df.Symbol.values[0]
        lst_positions.append(df_pos.copy())

    # (toc:Save on one year)
    if len(lst_positions) == 0:
        print('Not enough data to concat')
        continue
    print('   temp concat for %s...' % year)
    df_positions = pds.concat(lst_positions, axis=0)
    storage = pds.HDFStore(fyear + '/net_positions.h5')
    storage['df_positions'] = df_positions
    storage.close()
    print('.. saved.')
    lst_all_years.append(df_positions.copy())

print('concat all %d years...' % len(lst_all_years))
df_all_years = pds.concat(lst_all_years, axis=0)
storage = pds.HDFStore( fdir + '/net_positions_all.h5')
storage['df_all_years'] = df_all_years
storage.close()
print('Saved... All Done, try:')
print('ls -larth %s' % fdir + '/net_positions_all.h5')
## END
