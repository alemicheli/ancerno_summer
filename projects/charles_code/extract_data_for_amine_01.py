## Simply put face to face market data (from CFM) and Ancerno data
import helpers.marketdata as mdata

## (toc:Referencial)

class Referencial(object):

    def __init__(self, ancerno_dir=None, pool='US', version='preprocessed'):
        """
        pool can be 'US' or 'Non US'
        """
        if ancerno_dir is None:
            import os
            self.ancerno = os.environ['ANCERNO']
        else:
            self.ancerno = ancerno_dir
        self.pool = pool
        self.metaorders_dir = self.ancerno + ('/data/%s/%s Trading Equity Data/' % (version, pool))
        self.marketdata_dir = self.ancerno + ('/data/preprocessed/market_data/%s_equity/' % (pool))

        self.data = {}
        
    def read_year(self, year=None, max_stocks=None, symbols=None):
        """
        Example

        >>> years = ref.read_year()
        >>> df_year = ref.read_year(years[2])
        """
        import glob
        import os
        if year is None:
            print('You whould give me a year, please choose in:')
            md_years = [y.split('_')[-1].split('.')[0] for y in glob.glob(self.marketdata_dir + '*_year_*.pk')]
            mo_years = [u.split('/')[-2] for u in glob.glob(self.metaorders_dir + '*/')]
            common_years = sorted([int(y) for y in md_years if y in mo_years])
            print(common_years)
            return common_years
        print('Reading files for year <%d>...' % year)
        import pandas as pds
        print('... marketdata')
        df_marketdata = pds.read_pickle(self.marketdata_dir + ('%s_year_%d.pk' % (self.pool.replace(' ', '_'), year)))
        # I have to clean these data...
        # warning: I have two columns "eqt_id"!
        # df_meta['date'] = pds.to_datetime(df_meta.TradeDate)
        
        df_marketdata = mdata.clean_data(df_marketdata)
        u_symbols = df_marketdata['code'].unique()
        if symbols is not None:
            print('replacing %d symbols by %s.' % (len(u_symbols), symbols))
            u_symbols = symbols
        dic_symbols = {}
        print('... metaorders (%d symbols to read)' % len(u_symbols))
        for i, symbol in enumerate(u_symbols):
            if max_stocks is not None and i >= max_stocks:
                print("Stopping at %d/%d because you asked me to." % (i, len(u_symbols)))
                break
            fname = self.metaorders_dir + ('%d/merged_stock_%s.pkl' % (year, symbol))
            if not os.path.isfile(fname):
                fname = self.metaorders_dir + ('%d/merged_stock_%s.pkl' % (year, symbol.replace('@', '')))
                if not os.path.isfile(fname):
                    print(' <-- file <%s> is missing...\n --> skipping the symbol...' % fname)
                    continue
            if i % 20 == 0:
                print('   [%3d:%d] reading <%s>...' % (i, len(u_symbols), fname))
            df_symbol = pds.read_pickle(fname)
            # HERE I SHOULD MERGE THE METAORDERS (see with Frederic)
            if i % 20 == 0:
                print('   ... %d x %d data found in it.' % tuple(df_symbol.shape))
            # just for a fast check
            dic_symbols[symbol] = df_symbol.copy()
            
        return df_marketdata, dic_symbols
            
    def extract_rawdata(self, year=None, keep_cols=None,
                        symbols=None, out_pattern=None):
        """
        Example

        >>> listed_files = ref.extract_rawdata(year=2011, keep_cols=[...],
        ...                       out_pattern='/data/tmp/exports/small_datasets')
        """
        import glob
        import os
        if year is None:
            print('You whould give me a year, please choose in:')
            md_years = [y.split('_')[-1].split('.')[0] for y in glob.glob(self.marketdata_dir + '*_year_*.pk')]
            mo_years = [u.split('/')[-2] for u in glob.glob(self.metaorders_dir + '*/')]
            common_years = sorted([int(y) for y in md_years if y in mo_years])
            print(common_years)
            return common_years
        print('Reading files for year <%d>...' % year)
        ydir = ref.ancerno + out_pattern + '/%d/' % year
        if not os.path.exists(ydir):
            print('creating dir <%s>' % ydir)
            os.makedirs(ydir)
        import pandas as pds
        print('... raw data')
        files = glob.glob(self.ancerno + '/data/tmp/US Trading Equity Data/%d/stock_*.h*' % year)
        print('... %d files found' % len(files))
        for f in files:
            out_file = f.split('/')[-1].replace('stock_', 'filtered_').split('.')[0]
            print('... working on file <%s>' % f)
            store = pds.HDFStore(f)
            lst_df = []
            for day in store.keys():
                df = store[day]
                if keep_cols is not None:
                    df = df[keep_cols]
                lst_df.append(df)
            del store
            df_all = pds.concat(lst_df, axis=0)
            df_all.to_pickle(ydir + out_file + '.pkl')
            del df_all

        return files
            
## Test

pool = 'US'
ref = Referencial(version='tmp', pool=pool)
df_data, dic_o = ref.read_year(2011, symbols=['DELL'])  # , max_stocks=2)
# df = dic_o['DELL'][['oNumber', 'ov', 'TradeDate']]

##

keep_cols = ['BrokerCode',
             'OrderId',
             'Stockkey',
             'Symbol',
             'Cusip',
             'Currency',
             'TradeDate',
             'Side',
             'Price',
             'Volume',
             'CommissionUSD',
             'TaxesAndFees',
             'xv',
             'dv',
             'oNumber',
             'odtOrderDate',
             'odtLastDate',
             'ov',
             ]
import os

pool = 'US'
ref = Referencial(version='tmp', pool=pool)
# DONE: 2012, 2013, 2014, 2015
for year in [2010]:
    print('working on <%s>' % year)
    listed_files = ref.extract_rawdata(
        year=year, keep_cols=keep_cols,
        out_pattern='/data/tmp/exports/small_datasets')


## Remove the stocks with too many differences of prices
import pandas as pds

scp: Data/2012/*: No such file or directory


scp: Data/2012/*: No such file or directory


scp: Data/2012/*: No such file or directory

dic_quality = []
for code, df_meta in dic_o.items():
    #df_meta = dic_o['SLB']
    df_data_days = df_data[df_data.code == code]
    dic_quality = merge_md_and_mo(df_data_days, df_meta, dic_quality)
    print('done for <%s>' % code)
    
df_quality = pds.DataFrame(dic_quality)
##
import helpers.plot as hplot
# hplot.plot_config()
import matplotlib.pyplot as plt

f = plt.figure(0)
plt.clf()
ax1 = plt.subplot(1, 1, 1)
df_test = df_quality.sort('frac_bad', ascending=False).set_index('best name')[['frac_bad']].rename(columns={'frac_bad': 'bad frac'})
#plt.plot(df_test.values, '-', lw=4,alpha=.7)

(100. * df_test).ix[:30].plot.barh(ax=ax1,style='-', lw=0,alpha=.7)

plt.title('Worst price matches (pct)', size=18)
plt.ylabel('')
plt.grid(True)
plt.tight_layout()

if False:
    figname = 'p9_worst_price_matches_%s' % pool
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % figname)
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % figname)

## add 2 'quality' columns
# - one for the intraday
# - one for the trades of the day
# (once I kept some stocks)

##
import matplotlib.pyplot as plt
import pandas as pds

f = plt.figure(0)

for symbol, df_o in {}:  # dic_o.items():
    df_d = df_data[df_data.code == symbol]
    if df_d.shape[0] == 0:
        print('no data for <%s>... skipping...' % symbol)
        continue
    plt.clf()

    ax1 = plt.subplot(1, 1, 1)
    # problem: I have two columns named "date"
    #df_d.set_index(df_d.date.iloc[:,0])[['close', 'high', 'low', 'close']].plot(ax=ax1)
    df_d.set_index('date')[['open', 'high', 'low', 'close']].plot(ax=ax1)
    df_o.set_index(pds.to_datetime(df_o.TradeDate))[['Price']].plot(ax=ax1,style='ok', alpha=.5)

    plt.title('[%s] %s' % (symbol, df_d.cmp_name.unique()[0]), size=22)
    plt.grid()

    plt.tight_layout()
    
    if True:
        plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/p09_test_marketdata_%s_01.png' % symbol)
##
