CONVERT FILES
==============

I have a python script ``read_files_01.py`` and an awk script ``split_by_day.awk``:

- the python script goes towards all subdirectories of a head data directory
  1. unzip the ancerno ``original`` file in the corresponding ``tmp`` file
  2. split it in one file per day (using the awk script)
  3. convert
