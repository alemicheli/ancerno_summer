BEGIN {
    first_line = "";
    FS = "|";
};
{
    if(first_line==""){
	first_line = $0;
    } else {
	if(length($0)>0){
	    n = split($10, line_date, " ");
	    this_day = line_date[1];
	    if(!(this_day in all_days)) {
		if(length(this_day)==0) {
		    this_day = "UNKNOWN";
		}
		print "NEW DAY:" this_day ":";
		all_days[this_day] = 1;
		print first_line >> this_day "_split.txt";
	    }
	    print $0 >> this_day "_split.txt";
	    all_days[this_day] = all_days[this_day] +1;
	}
    }
}
END{
    for(day in all_days){
	print day "," all_days[day] >> substr(day, 1, 7) "_summary.txt";
    }
}

