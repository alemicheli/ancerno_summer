## Stats on summaries
import os
import glob
import pandas as pds

pool = 'US'

fdir = os.environ['ANCERNO'] + '/data/tmp/%s Trading Equity Data/' % pool

for year in glob.glob(fdir + '/*'):
    this_year = year.split('/')[-1]
    print("working on year <%s>" % this_year)

    df_all_summaries = pds.DataFrame()
    for summary_file in glob.glob(year + '/*_summary.txt'):
        print("   read <%s>" % summary_file.split('/')[-1])
        df_summary = pds.read_csv( summary_file, names=['date', 'Nbe metaorders'])
        df_all_summaries = pds.concat([df_all_summaries, df_summary], axis=0, ignore_index=True)
        print("   ---> %d lines" % df_all_summaries.shape[0])
    yfile = os.environ['ANCERNO'] + ('/data/tmp/%s_%s.pkl' % (pool.replace(' ', '_'), this_year))
    # print('saving in <%s>' % yfile)
    df_all_summaries.to_pickle(yfile)

## Now on all files
import os
import glob
import pandas as pds

df_years = pds.DataFrame()
for year_file in glob.glob(os.environ['ANCERNO'] + ('/data/tmp/%s_*.pkl' % pool.replace(' ', '_'))):
    df_years = pds.concat([df_years, pds.read_pickle(year_file)], axis=0, ignore_index=True)

print(df_years.shape)
df_unknown = df_years[df_years.date == 'UNKNOWN']
df_years = df_years[df_years.date != 'UNKNOWN']
df_years['date'] = pds.to_datetime(df_years.date.values)

##
import matplotlib.pyplot as plt

f = plt.figure(0)
plt.clf()

from matplotlib.dates import MonthLocator, WeekdayLocator, DateFormatter
months = MonthLocator(range(1, 13), bymonthday=1, interval=3)
monthsFmt = DateFormatter("%y %m")

ax1 = plt.subplot(1,1,1)
df_years.set_index('date').sort_index(inplace=False).plot(ax=ax1, lw=4, alpha=.75)
ax1.xaxis.set_major_locator(months)
ax1.xaxis.set_major_formatter(monthsFmt)
ax1.autoscale_view()

plt.grid(True)
f.autofmt_xdate()
plt.xticks(rotation='vertical')
plt.xlabel('')

plt.title('Number of Converted Metaorder in the US dataset', size=20)
plt.tight_layout()

if False:
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/p02_nbe_us.png')

## END
