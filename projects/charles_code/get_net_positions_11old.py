## Get net positions computed by 10.py
pool = 'US'

## (toc:Imports and directories)
import glob
import os

import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

#
all_stored = fdir + 'net_positions'

# Here I only keep recent ones
lst_all_years = []
for fyear in glob.glob(fdir + '201*/'):
    fname = fyear + '/net_positions.h5'
    if not os.path.isfile(fname):
        print('no file in <%s>...' % fyear)
        continue
    print('working on <%s>...' % fyear)
    storage = pds.HDFStore(fname)
    df_pos  = storage['df_positions']
    storage.close()
    lst_all_years.append(df_pos.copy())
df_all_years = pds.concat(lst_all_years, axis=0)

## (toc:Worst cases)

# (toc:Simple ref stats:-)
df_all_years['ratio'] = df_all_years['net'] / df_all_years['sum']
df_all_years['year'] = [d.year for d in df_all_years.index.values]
df_avg_ratio = df_all_years.groupby(['Symbol', 'year'])[['ratio', 'sum']].mean()
df_std_ratio = df_all_years.groupby(['Symbol', 'year'])[['ratio', 'sum']].std()
df_count_ratio = df_all_years.groupby(['Symbol', 'year'])[['ratio']].count().rename(columns={'ratio': 'year (count)'})

df_ = pds.merge(df_std_ratio, df_avg_ratio,
                left_index=True, right_index=True,
                how='outer', suffixes=[' (std)', ' (avg)'])
df_ = pds.merge(df_, df_count_ratio,
                left_index=True, right_index=True,
                how='outer')

df_all_years = pds.merge(df_all_years, df_.reset_index(),
                left_on=['Symbol', 'year'], right_on=['Symbol', 'year'],
                how='left')

## (toc:Identify extremes:-)
df_all_years['abnormal sum'] = (df_all_years['sum'] / df_all_years['sum (avg)']) 
df_all_years['abnormal ratio'] = (df_all_years['ratio'] - df_all_years['ratio (avg)']) / df_all_years['ratio (std)']

# Take the most intense ratios
ind_sr = np.argsort(-np.abs(df_all_years['abnormal ratio'].values))
# df_all_years.ix[df_all_years.index[ind_s]]
df_all_years['ranked ab.ratio'] = ind_sr

# Take the most intense ratios
ind_ss = np.argsort(-np.abs(df_all_years['abnormal sum'].values))
# df_all_years.ix[df_all_years.index[ind_s]]
df_all_years['ranked ab.sum'] = ind_ss

## select
K1 = 800
# K2 = 500
df_all_trunc = df_all_years.sort('abnormal ratio', inplace=False, ascending=False).head(K1)
# df_selected = df_all_trunc[df_all_trunc['ranked ab.sum'] < K2]
df_selected = df_all_trunc[df_all_trunc['abnormal sum'] > 3]

sum_thresholds = np.linspace(2, 20, 20)
df_test_thres = pds.DataFrame({'threshold': sum_thresholds, 'nbe': [(df_all_trunc['abnormal sum'] > t).sum() for t in sum_thresholds]})

## END
