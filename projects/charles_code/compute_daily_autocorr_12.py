## Get net positions computed by 10.py

# [toc:
# * Imports and directories
#   * Preprocessings
#   * Simple ref stats
# * Autocorr
#   * A little bit of cleanup
#   * Plot Avg Autocorr
# ]

pool = 'US'

# (toc:Imports and directories)
import glob
import os

import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

# (toc:Preprocessings:-)
all_stored = fdir + 'net_positions.h5'
# at one point I will taje net_positions_all.h5 (here I only have 2000+ years
storage = pds.HDFStore(all_stored)
df_all_years = storage['df_all_years']
storage.close()
df_all_years = df_all_years.set_index('TradeDate')
df_all_years['year'] = [d.year for d in df_all_years.index.values]
df_all_years['ratio'] = df_all_years['net'] / df_all_years['sum']

df_recent = df_all_years.copy() # [df_all_years.index > pds.to_datetime('2012-01-01').date()]

# (toc:Simple ref stats:-)
df_avg_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio', 'sum']].mean()
df_std_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio', 'sum']].std()
df_count_ratio = df_recent.groupby(['Symbol', 'year'])[['ratio']].count().rename(columns={'ratio': 'year (count)'})

df_ = pds.merge(df_std_ratio, df_avg_ratio,
                left_index=True, right_index=True,
                how='outer', suffixes=[' (std)', ' (avg)'])
df_ = pds.merge(df_, df_count_ratio,
                left_index=True, right_index=True,
                how='outer')

df_recent = pds.merge(df_recent.reset_index(), df_.reset_index(),
                left_on=['Symbol', 'year'], right_on=['Symbol', 'year'],
                how='left')

df_recent['abnormal sum'] = (df_recent['sum'] / df_recent['sum (avg)']) 
df_recent['abnormal ratio'] = (df_recent['ratio'] - df_recent['ratio (avg)']) / df_recent['ratio (std)']

## (toc:Autocorr)

lst_autocorr = []
for symbol, df in df_recent.groupby('Symbol'):
    print('Working on <%s>' % symbol)
    df_ = df.set_index('TradeDate').sort()
    dic_stats = {'Symbol': symbol, 'nbe': df_.shape[0]}
    dic_stats.update({('ratio (norm) lag=%d' % d, d): df_['abnormal ratio'].autocorr(lag=d) \
                       for d in [1, 2, 3, 4, 5, 6, 7, 10, 20, 60]})
    dic_stats.update({('volumes (norm) lag=%d' % d, d): df_['abnormal sum'].autocorr(lag=d) \
                       for d in [1, 2, 3, 4, 5, 6, 7, 10, 20, 60]})
    
    lst_autocorr.append(dict(dic_stats))
                         
df_autocorr = pds.DataFrame(lst_autocorr)

# (toc:A little bit of cleanup:-)
col2num = lambda df: df.rename(columns={c: c[1] for c in df.columns})

df_ratio_ac   = col2num(df_autocorr[[c for c in df_autocorr.columns if c[0].find('ratio') == 0]])
df_volumes_ac = col2num(df_autocorr[[c for c in df_autocorr.columns if c[0].find('volum') == 0]])

## (toc:Plot Avg Autocorr:-)
import matplotlib.pyplot as plt

fig = plt.figure(0)
plt.clf()

# cm=cplot.colormap_from_list(lst_k, colorscheme='RdBu')

ax1 = plt.subplot(2, 1, 1)
df_ratio_ac.T.sort().plot(lw=2, alpha=.25, ax=ax1)
df_ratio_ac.T.sort().mean(axis=1).plot(lw=5, alpha=.95, ax=ax1, style='-k')
plt.ylabel('Autocorr of signed ratio', size=18)
ax1.legend_.remove()
plt.grid(True)
plt.ylim((-.2,.5))
plt.xlim((1,30))

ax2 = plt.subplot(2, 1, 2)
df_volumes_ac.T.sort().plot(lw=2, alpha=.25, ax=ax2)
df_volumes_ac.T.sort().mean(axis=1).plot(lw=5, alpha=.95, ax=ax2, style='-k')
plt.ylabel('Autocorr of volumes', size=18)
plt.xlabel('Lag (days)', size=18)
ax2.legend_.remove()
plt.grid(True)
plt.ylim((-.2,.5))
plt.xlim((1,30))

plt.tight_layout()
# plt.subplots_adjust(right=0.95)
if False:
    img_name = 'p12_autocorr'
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % img_name)
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % img_name)

## (toc:Plot two levels of anomaly:-)
import matplotlib.pyplot as plt

fig = plt.figure(1)
plt.clf()

# cm=cplot.colormap_from_list(lst_k, colorscheme='RdBu')

ax1 = plt.subplot(1, 1, 1)
plt.plot(df_recent['abnormal sum'].values, np.abs(df_recent['abnormal ratio'].values), 'ok', alpha=.01)
ax1.set_xscale('log')
plt.title('%d points over %d stocks' % (df_recent.shape[0], df_recent.Symbol.unique().shape[0]), size=18)
plt.xlabel('Volume ratio', size=18)
plt.ylabel('Abnormal |net|', size=18)
plt.grid(True)
plt.xlim((1e-6,1e3))
plt.ylim((0,3))

plt.tight_layout()
# plt.subplots_adjust(right=0.95)
if False:
    img_name = 'p12_joined_distrib'
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % img_name)
    plt.savefig('/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % img_name)

    

## END
