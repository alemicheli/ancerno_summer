## Explore the content of variable

# from __future__ import print_function

pool = 'US'
year = '2012'

##
# df_v = pds.concat({'sum': df_meta.groupby(['date', 'OrderId'])[['Volume']].sum(), 'max': df_meta.groupby(['date', 'OrderId'])[['xv', 'ov', 'dv']].max(), 'count': df_meta.groupby(['date', 'OrderId'])[['xv']].count()}, axis=1)
# df_v.columns = ["%s %s" % (a,b) for a,b in zip(df_v.columns.droplevel(level=1), df_v.columns.droplevel(level=0)) ]

# ov > dv when on several days

# test_names = glob.glob('/home/silo2/ancerno/AncernoDB/data/tmp/US Trading Equity Data/2012/*.pk')

## (toc:Imports and directories
import glob
import os

import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)
outprefix = os.environ['ANCERNO'] + ('/data/tmp/reports/%s_%s' % (pool, year))

fnames = glob.glob(fdir + year + '/merged_*.pkl')

print('On year %s, universe is made of %d stocks' % (year, len(fnames)))
## (toc:Read Ancerno ref file)
ref_file = '/home/ma/c/clehalle/dev_python/projects/ancerno/data/TradesDataDictionary.csv'
df_ref = pds.read_csv(ref_file, names=['code', 'type', 'description', 'ref_filename'])
print('%d references' % df_ref.shape[0])
## (toc:Read CFM ref file)
json_universe = fdir.replace('%s Trading Equity Data/' % pool,
                             '%s_stocks_1997-2015_found.json' % pool.replace(' ', '_'))
df_universe = pds.read_json( json_universe)
print('Universe dic is of size %d' % df_universe.shape[0])

## (toc:Read one random file)
from numpy.random import choice

#fname   = choice(fnames)
for fname in fnames:
    print('<%s> chosen' % fname)

    symbol  = fname.split('_')[-1].split('.')[0]
    outprefix = os.environ['ANCERNO'] + ('/data/tmp/reports/%s_%s' % (pool, year))

    log = open(outprefix + ('_%s.txt' % symbol), "w")

    df_info = df_universe[df_universe.code == symbol]
    if df_info.shape[0] != 1:
        log.write("Problem with <%s>: %d matche(s)...\n" % (symbol, df_info.shape[0]))
    dic_info = {c: df_info[c].values[0] for c in df_info.columns}

    import pprint
    log.write("INFO:\n")
    pprint.pprint(dic_info)
    log.write("%s\n" % dic_info)

    print("loading data...")
    df_meta = pds.read_pickle(fname)

    print("%s: %d meta over %d variables" % (symbol, df_meta.shape[0], df_meta.shape[1]))
    log.write("%s: %d meta over %d variables\n" % (symbol, df_meta.shape[0], df_meta.shape[1]))

    ## (toc:Match names and description)
    dic_desc = {}
    dic_type = {}

    def conv_code(c):
        dic_conv = {'CommissionUSD': 'Commission'}
        return dic_conv.get(c, c)

    for c in df_meta.columns:
        c_ = conv_code(c)
        desc = df_ref[df_ref.code.str.lower() == c_.lower()]
        dic_desc[c] = desc.description.values[0]
        dic_type[c] = desc['type'].values[0].strip()
        if dic_type[c] == 'int':
            print('convert <%s> to INT' % c)
            df_meta[c] = df_meta[c].astype(int)

    # df_dv = df_meta.groupby('ORderID')['dv'].sum()

    ##
    print('---')

    for c in df_meta.columns:
        ts_nonan = df_meta[c].dropna()
        log.write("\n----------------------\n")
        log.write("***%s***\n" % c)
        log.write("TYPE=%s\n" % dic_type[c])
        log.write("%s\n" % dic_desc[c])

        print("%s: %d NaNs" % (c, df_meta.shape[0] - len(ts_nonan)))
        log.write("%s: %d NaNs\n" % (c, df_meta.shape[0] - len(ts_nonan)))

        if c.lower() in ['clienttype', 'stockstyle', 'goodtrade', 'side']:
            stats = df_meta.groupby(c)['Symbol'].count()
            print(stats)
            log.write("%s\n" % stats)
        else:
            print(ts_nonan.describe())
            log.write("%s\n" % ts_nonan.describe())

    log.flush()

    ## (toc:Durations)
    df_meta['date'] = [d.date() for d in pds.to_datetime(df_meta.ddtE)]


    ## (toc:Specific study of OrderId)

    lst_strange = []
    for i, row in df_meta[['OrderId', 'date']].iterrows():
        v = row['OrderId']
        if isinstance(v, str) and v.find(':')>0:
            lst_strange.append({'id': i, 'date': row['date'], 'OrderID': v})

    df_strange = pds.DataFrame(lst_strange)
    df_compare = pds.DataFrame({'OID-date': df_strange.groupby('date')['id'].count(),
                                'all': df_meta.groupby('date')['OrderId'].count()}).fillna(0)
    print('Avgerage ratio: %.2f%%' % (100. * df_compare['OID-date'] / df_compare['all']).mean())
    print('Overall  ratio: %.2f%%' % (100. * (df_compare['OID-date']).sum() / (df_compare['all']).sum()))

    ## (toc:Durations)
    df_meta['duration'] = df_meta['xdtX'] - df_meta['xdtP']
    df_meta['duration (h)'] = df_meta['duration'] / pds.Timedelta(1, unit='h')

    ## (toc:When OrderId is available I need to merge the lines)

    #df_check = df_meta.groupby(['date', 'Side', 'xv', 'ClientType'])[['OrderId']].count().sort_values('OrderId', ascending=False)
    gr = df_meta.groupby(['date', 'Side', 'xv', 'ov', 'ClientType', 'xdtP', 'odtET', 'ddtX', 'duration',
                                'BrokerCode', 'Price'])
    df_check = pds.DataFrame({'Count': gr['OrderId'].count(), 'duration (h)': gr['duration (h)'].mean()})
    df_check = df_check.sort_values('Count', ascending=False)

    print("Ambiguities:")
    print(df_check.reset_index()[['date','xv','xdtP','Count','duration (h)']].head(12))
    log.write('%d AMBIGUITITES:\n' % df_check.shape[0])
    log.write('%s\n' % df_check.reset_index()[['date','xv','xdtP','Count','duration (h)']].head(12))
    ##
    import matplotlib.pyplot as plt

    f = plt.figure(0)
    plt.clf()
    ax_ = plt.subplot(1,1,1)
    df_check.hist(ax=ax_, bins=60)
    ax1 = plt.subplot(1,2,1)
    nbe_same = (df_check['Count']==1).sum()
    plt.title('Distribution of original nbe of orders\nin the new classification\n%d (over %d) are exactly the same' % (nbe_same, len(df_meta.OrderId.unique())))
    ax2 = plt.subplot(1,2,2)
    nbe_small = (df_check['duration (h)'] < 1. / 60.).sum()
    plt.title("%d 'metaorders' over %s for %s\n%d lasted less that 1 minute" % (df_check.shape[0], year,
                                                                                    symbol, nbe_small))
    plt.tight_layout()

    if True:
        img_name = 'p07_distrib_duration_%s_%s' % (year, symbol)
        plt.savefig( '/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.png' % img_name)
        plt.savefig( '/home/ma/c/clehalle/dev_python/projects/ancerno/images/%s.pdf' % img_name)

    ##
    K = 0
    cnames = df_check.index.names
    df_select_meta = df_meta.copy()
    for c in cnames:
        df_select_meta = df_select_meta[df_select_meta[c] == df_check.reset_index()[c].values[K]]

    print(df_select_meta[['onDays', 'OrderId', 'xp', 'xv', 'Volume', 'Price', 'xpX', 'oNumber']])

    ##
    #df_test = df_meta[df_meta.OrderId=='000000378117702']

    # df_meta[df_meta['date'] == pds.to_datetime('2012-05-30').date()]

    ## (toc:End of report)

    log.close()
## END
