#!/usr/bin/env python

# good tutorial here:
# https://docs.python.org/2/howto/argparse.html
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("year", help="year to parse")
parser.add_argument("--pool", default="US", help="pool to extract [US|Non US]")
parser.add_argument("--months", default=None, help="restrict the extraction to those months")
args = parser.parse_args()

# usage: convert_one_year.py 2010 [01,02,03]
print("----------------------------------------------")
## Convert files in pk
#import sys
#print("args: %s" % sys.argv)
str_year = args.year  # sys.argv[1]
#if len(sys.argv) > 2:
#    selected_months = sys.argv[2].split(',')
#    print("I have one argument more: months=%s (separator=',')" % selected_months)
#else:
#    selected_months = None
selected_months = args.months
pool = args.pool

print("%s|%s|%s" % (str_year, pool, selected_months))

## (toc:Use numpy txt reader)
# This is the good solution:
# 1. use numpy file reader (it knows how to handle \x0)
# 2. then convert empty spaces to NaNs
import os
ancerno_dir = os.environ['ANCERNO']
original_data = '/data/original/%s Trading Equity Data/' % pool

## (toc:LIB)
def destination_file(fpath):
    import distutils.dir_util
    dpath = fpath.replace('original', 'tmp')
    dname = os.path.dirname(dpath)
    distutils.dir_util.mkpath(dname)
    return dpath, dname


def convert_file(uncomp_fname):
    import numpy as np
    import pandas as pds
    ar_data = np.genfromtxt(uncomp_fname, delimiter='|',
                            dtype=None, names=True, missing_values=np.nan)

    df_data = pds.DataFrame(ar_data)
    df_data_with_nans = df_data.replace(r'', np.nan, regex=True)
    for c in df_data_with_nans.columns:
        if c.find('dt') != 1:
            continue
        df_data_with_nans[c] = pds.to_datetime(df_data_with_nans[c])
    
    df_data_with_nans.to_pickle(uncomp_fname.replace('txt', 'pk'))
    return df_data_with_nans.shape

## Unzip
import glob

dname = ancerno_dir + original_data + str_year
year = int(dname.split('/')[-1])
print("working on year %d" % (year))
for fname in glob.glob(dname + '/*.txt.gz'):
    this_month = fname.split('/')[-1].split('.')[0][-2:]
    print('---> working month: [%s]' % this_month)
    
    if (selected_months is not None) and (this_month not in selected_months) :
        print("[%s] not in the selected list... skipping" % (this_month))
        continue
    dest_fname, dest_dir = destination_file(fname)
    print("%s --> %s" % (fname, dest_dir))
    print("   UNZIP")
    os.system('unzip -d "%s/" "%s"' % (dest_dir, fname))
    print("   SPLIT")
    new_fname = fname.replace('.gz', '').split('/')[-1]
    os.system('cd "%s";awk -f ~/dev_python/projects/ancerno/split_by_day.awk %s' % (dest_dir, new_fname))
    print("   CONVERT")
    for split_fname in glob.glob(dest_dir + '/*_split.txt'):
        print("   ... work on <%s>" % split_fname)
        try:
            shape = convert_file(split_fname)
            print("   ___ %d x %d" % shape)
        except:
            with open("%s/problems.txt" % dest_dir, "a") as problems:
                problems.write("problem with file <%s>\n" % split_fname)
    print("   REMOVE")
    os.system('cd "%s";rm *_split.txt' % dest_dir)
    os.system('cd "%s";rm *Trades*.txt' % dest_dir)

    
## END
