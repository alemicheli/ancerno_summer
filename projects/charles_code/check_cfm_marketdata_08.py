##
import glob

fdir = '/home/silo2/ancerno/AncernoDB/data/preprocessed/market_data/US_equity/'

fnames = glob.glob(fdir + '*_year_*.pk')

def numeric_sort(text):
    def atoi(txt):
        return int(txt) if txt.isdigit() else txt
    import re
    return [atoi(f) for f in re.split('(\d+)', text)]

fnames.sort(key=numeric_sort)
for fname in fnames:
    print(fname)
##
import pandas as pds

for fname in fnames:
    df = pds.read_pickle(fname)
    print('%s --> %d x %d' % (fname.split('/')[-1].split('.')[0], df.shape[0], df.shape[1]))


## END
