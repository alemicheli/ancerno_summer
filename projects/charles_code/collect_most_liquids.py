#!/usr/bin/env python

# good tutorial here:
# https://docs.python.org/2/howto/argparse.html
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("year", help="year to parse")
parser.add_argument("--pool", default="US", help="pool to extract [US|Non US]")
parser.add_argument("--months", default=None, help="restrict the extraction to those months")
parser.add_argument("-b", "--select-bests", default=200, help="number of most traded stocks to keep each day")
parser.add_argument("-s", "--packets-size", default=10, help="reduce the summary ever packets-size days")
args = parser.parse_args()

str_year = args.year
selected_months = args.months
pool = args.pool
select_bests = args.select_bests
packets_size = args.packets_size

print("Y=%s|P=%s|M=%s|b=%s|s=%s" % (str_year, pool, selected_months, select_bests, packets_size))

## (toc:Imports)
# Simple stats to find the most liquid stocks / guys
import os
import glob
import pandas as pds

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

## (toc:LIB)

def groupby_daily(df_data):
    return df_data.groupby('Symbol')[['Value', 'has_taxes', 'count']].sum().sort_values(by='Value', ascending=False)

def reduce_day(df_data):
    df_data['Value'] = df_data['Price'] * df_data['Volume']
    df_data['has_taxes'] = (df_data['TaxesAndFees']>0).astype(int)
    df_data['count'] = 1
    df_summary = groupby_daily(df_data)

    print("Reduction from %d rows to %d ones..." % (df_data.shape[0], df_summary.shape[0]))

    return df_summary
    
## (toc:The main loop)
ydir = fdir + str_year

fnames = glob.glob( ydir + '/*.pk')
print(len(fnames))

df_all_summaries = pds.DataFrame()
for i, fname in enumerate(fnames):
    df_data = pds.read_pickle(fname)
    df_summary = reduce_day(df_data).reset_index().ix[:select_bests]
    df_concat  = pds.concat([df_all_summaries, df_summary], axis=0, ignore_index=True)
    df_all_summaries = groupby_daily(df_concat.reset_index()).reset_index()

    if i % packets_size == 0:
        print("%d/%d: down-sizing from %d to %d" % (i, len(fnames), df_all_summaries.shape[0], select_bests))
        df_all_summaries = df_all_summaries.ix[:select_bests]

df_all_summaries.to_pickle(ydir + '_stocks.pkl')
## END
