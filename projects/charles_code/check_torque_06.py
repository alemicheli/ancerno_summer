#!/usr/bin/env python

# good tutorial here:
# https://docs.python.org/2/howto/argparse.html
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("year", help="year to parse")
parser.add_argument("--pool", default="US", help="pool to extract [US|Non US]")
args = parser.parse_args()

year = args.year
pool = args.pool

## Check Torque generated the expected files

import os
import glob
import pandas as pds
import numpy as np

fdir = os.environ['ANCERNO'] + ('/data/tmp/%s Trading Equity Data/' % pool)

## (toc:Get Universe)
json_universe = fdir.replace('%s Trading Equity Data/' % pool,
                             '%s_stocks_1997-2015_found.json' % pool.replace(' ', '_'))
df_universe = pds.read_json( json_universe)
print("Universe of %d stocks to concat..." % df_universe.shape[0])

##
import os
dic_hd5 = {}
for fname in glob.glob(fdir + ('%s/stock_*.hd5' % year)):
    dic_hd5[fname.split('/')[-1].split('.')[0].split('_')[-1]] = os.path.getsize(fname)
dic_pkl = {}
for fname in glob.glob(fdir + ('%s/merged_stock_*.pkl' % year)):
    dic_pkl[fname.split('/')[-1].split('.')[0].split('_')[-1]] = os.path.getsize(fname)
    

## Compare
lst_all = []
for k, sh in dic_hd5.items():
    sp = dic_pkl.get(k, -1)
    lst_all.append({'fname': k, 'hd5_size': sh, 'pkl_size': sp})

df_all = pds.DataFrame(lst_all)

print('%s: %d stock found over %d, %d not in merged files' % (year, len(lst_all), df_universe.shape[0], (df_all.pkl_size<0).sum()))

## END
