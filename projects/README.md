#This repository contains the development code for the Ancerno Database. 

The folder **filter_dev** contains the tools for filtering a dataframe. 

The folder **minutes** contains the minutes from the weekly meetings.

The plots for comparing the stat for metaorder and child orders are in the **fractional_volume.ipynb** in the folder **statistics_metaorder_zarinelli**
