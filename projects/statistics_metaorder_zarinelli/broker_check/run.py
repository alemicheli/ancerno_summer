import check_tools as tl
from glob import glob
from multiprocessing import Pool

root_dir="/home/ma/a/am1118/data/original/US Trading Equity Data/"


if __name__ == "__main__":
        pool_list = tl.make_pool_list(root_dir)
        pool = Pool()
        result_pairs = pool.map(tl.check_broker,pool_list)

        tl.to_file("broker_orderid.txt", result_pairs)
