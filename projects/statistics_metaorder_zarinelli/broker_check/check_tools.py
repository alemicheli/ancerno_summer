import pandas as pd
import glob
from pathlib import Path

def to_file(in_file, in_list):
    f = open(in_file, "w+")
    for pair in in_list:
        f.write("Filename: {}, Indices: {} \n".format(pair[0],str(pair[1])))
    
    f.close()

def check_broker(in_file):
    #load file
    local_df=pd.read_csv(in_file, sep="|", compression = "gzip")
    #drop nan to have clean columns
    local_df.dropna(axis=0, subset=["BrokerCode","OrderId"], inplace=True)
    #we group by OrderId (use OrderId as index)
    check = local_df[["BrokerCode","OrderId"]].groupby("OrderId")
    #of each orderId we count the number of unique BrokerCodes
    test = check["BrokerCode"].nunique().to_frame()
    #we drop those with brokercode=1
    no_one = test.drop(test[test["BrokerCode"]==1].index, inplace=False)
    to_print_name=  in_file.split("/")[-1]
    print(to_print_name+": Done")
    if no_one.empty:
        return (to_print_name,"None")
    else : 
        return (to_print_name, [test[test!=1].index])
    


def make_pool_list(in_dir):
    f_list=[]
    for sub_dir in  glob.glob(in_dir+"*"):
        for fname in glob.glob(sub_dir+"/*"):
            if "Trades201501.txt.gz" in fname: 
                print(fname)
                f_list.append(fname)
    return f_list